/*
 * Classe que defineix un dissenyador o dissenyadora. Un dissenyador o dissenyadora
 * es defineix pel seu nif, nom i estat, és a dir, actiu si està treballant o no 
 * actiu si està de baixa o vacances.
 */
package components;

/**
 *
 * @author root
 */
public class Dissenyador extends Treballador {
    
   
    /*
     TODO CONSTRUCTOR
     Paràmetres: valors per tots els atributs de la classe menys l'atribut actiu
     Accions:
     - Assignar als atributs els valors passats com a paràmetres, menys a actiu,
     ja que quan es crea un nou dissenyador o dissenyadora, el seu estat sempre es
     actiu, per tant li assignem verdader.
     */
    public Dissenyador(String nif, String nom, boolean actiu) {
        super(nif,nom,actiu);
    }

    /*
     TODO Mètodes accessors    
     */
    
    /*
     TODO
     Paràmetres: cap
     Accions:
     - Demanar a l'usuari les dades per consola per crear un nou dissenyador o nova
     dissenyadora. Les dades a demanar són les que passem per paràmetre en el constructor.
     - També heu de tenir en compte que tant el nom pot ser una frase, per exemple, 
     Francesc Xavier.
     Retorn: El nou dissenyador o nova dissenyadora creat/da.
     */
    public static Dissenyador addDissenyador() {

        String nif;
        String nom;
        int actiuNum;
        boolean actiu = false;

        System.out.println("NIF del dissenyador o dissenyadora:");
        nif = DADES.next();
        DADES.nextLine(); //Neteja buffer
        System.out.println("Nom del dissenyador o dissenyadora:");
        nom = DADES.nextLine();
        System.out.println("Esta actiu introdueix 1, en cas contrari introdueix 0");
        actiuNum = DADES.nextInt();
        DADES.nextLine(); //Neteja buffer
        if( actiuNum == 1 ){
            actiu = true;
        };
                
        return new Dissenyador(nif, nom , actiu);
    }

    /*
     TODO
     Paràmetres: cap
     Accions:
     - Demanar a l'usuari que introdueixi les noves dades de l'objecte actual
     i modificar els atributs corresponents d'aquest objecte.
     - En el cas de l'atribut actiu, li heu de demanar que si el dissenyador o dissenyadora està en 
     actiu introdueixi 1 i en cas contrari 0.
     - Li heu de mostrar a l'usuari els valors dels atributs abans de modificar-los.
     En el cas de l'atribut actiu, li heu de mostrar el missatge: "\nEl dissenyador
     o dissenyadora està en actiu", si el dissenyador o dissenyadora està en actiu,
     i en cas contrari, el misaatge "\nEl dissenyador o dissenyadora no està en actiu".
     Retorn: cap
     */
   
    
    public void showComponent(){
       super.showComponent("Dissenyador");
    }
    
    
    public void updateComponent(){
       super.updateComponent("Dissenyador");
    }
    
    
    
     
}
