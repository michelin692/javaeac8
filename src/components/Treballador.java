/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package components;
/**
 *
 * @author Miguel
 */
public abstract class Treballador implements Component{
       
    protected String nif;
    protected String nom;
    protected boolean actiu;

    public Treballador(String nif, String nom, boolean actiu) {
        this.nif = nif;
        this.nom = nom;
        this.actiu = actiu;
    }
    
    //metodes setters 

    public void setNif(String nif) {
        this.nif = nif;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setActiu(boolean actiu) {
        this.actiu = actiu;
    }
    //metodes getters
    public String getNif() {
        return nif;
    }

    public String getNom() {
        return nom;
    }

    public boolean isActiu() {
        return actiu;
    }
    
    
    public void showComponent( String tipoClase ){
       
        System.out.println("\nLes dades del "+tipoClase+ " o "+tipoClase+"a amb nif " + nif + " són:");
        System.out.println("\nNom: " + nom);
        System.out.println("\nL'estat és: ");

        if(actiu){
            System.out.print("Actiu");
        }else{
            System.out.print("No actiu");
        }
        
    }
    
    public void updateComponent( String tipoClase ){
        
        System.out.println("\nNIF del "+tipoClase+" o "+tipoClase+"a: " + nif);
        System.out.println("\nEntra el nou nif:");
        nif = DADES.next();
        DADES.nextLine(); //Neteja buffer
        System.out.println("\nNom "+tipoClase+" o "+tipoClase+"a: " + nom);
        System.out.println("\nEntra el nou nom:");
        nom = DADES.nextLine();
        
        if(actiu){
            System.out.println("\nEl "+tipoClase+" o "+tipoClase+"a està en actiu");
        }else{
            System.out.println("\nEl "+tipoClase+" o "+tipoClase+"a no està en actiu");
        }
        
        System.out.println("\nEntra el nou estat (1 si està en actiu o 0 en cas contrari):");
        
        int estat=DADES.nextInt();
        
        if(estat==1){
            actiu=true;
        }else{
            actiu=false;
        }
    }
    
    
}
