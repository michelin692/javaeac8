/*
 * Classe que defineix un jardiner o jardinera. Un jardiner o jardinera es defineix 
 * pel seu NIF, nom, torn de feina i estat, és a dir, actiu si està treballant o no actiu si està 
 * de baixa o vacances.
 */
package components;

/**
 *
 * @author root
 */
public class Jardiner extends Treballador {
    
    
    private Torn torn;

    /*
     TODO CONSTRUCTOR
     Paràmetres: valors per tots els atributs de la classe menys l'atribut actiu
     Accions:
     - Assignar als atributs els valors passats com a paràmetres, menys a actiu,
     ja que quan es crea un nou jardiner o jardinera, el seu estat sempre és
     actiu, per tant li assignem verdader. I torn, ja que quan es crea un nou
     jardiner o jardinera aquest encara no té assignat cap torn, per tant li assignarem
     null.
     */
    public Jardiner(String nif, String nom, boolean actiu) {
        super(nif,nom,actiu);
        this.torn = null;
    }

    /*
     TODO Mètodes accessors    
     */
   
    public Torn getTorn() {
        return torn;
    }

    public void setTorn(Torn torn) {
        this.torn = torn;
    }

    /*
     TODO
     Paràmetres: cap
     Accions:
     - Demanar a l'usuari les dades per consola per crear un nou jardiner o nova
     jardinera. Les dades a demanar són les que passem per paràmetre en el constructor.
     - També heu de tenir en compte que tant el nom pot ser una frase, per exemple, 
     Francesc Xavier.
     Retorn: El nou jardiner o nova jardinera creat/da.
     */
    public static Jardiner addJardiner() {

        String nif;
        String nom;
        Torn torn;
        int actiuNum;
        boolean actiu = false;

        System.out.println("NIF del jardiner o jardinera:");
        nif = DADES.next();
        DADES.nextLine(); //Neteja buffer
        System.out.println("Nom del jardiner o jardinera:");
        nom = DADES.nextLine();
        System.out.println("Esta actiu introdueix 1, en cas contrari introdueix 0");
        actiuNum = DADES.nextInt();
        DADES.nextLine(); //Neteja buffer
        if( actiuNum == 1 ){
            actiu = true;
        };
        return new Jardiner(nif, nom, actiu);
    }

    /*
     TODO
     Paràmetres: cap
     Accions:
     - Demanar a l'usuari que introdueixi les noves dades de l'objecte actual
     i modificar els atributs corresponents d'aquest objecte, menys de l'atribut torn.
     - En el cas de l'atribut actiu, li heu de demanar que si el jardiner o jardinera està en 
     actiu introdueixi 1 i en cas contrari 0.
     - Li heu de mostrar a l'usuari els valors dels atributs abans de modificar-los.
     En el cas de l'atribut actiu, li heu de mostrar el missatge: "\nEl jardiner o jardinera
     està en actiu", si el jardiner o jardinera està en actiu, i en cas contrari, el missatge 
     "\nEl jardiner o jardinera no està en actiu".
     Retorn: cap
     */
    
   
    public void showComponent(){
        super.showComponent("Jardiner");
    }
    
    public void updateComponent(){
       super.updateComponent("Jardiner");
    }
    
}
